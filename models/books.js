const tableName = 'books'

module.exports = {
    get : async (db, extendWhere = {}, callback) => {
        try {
            let books = await db(tableName).where((builder) => {
                if (Object.keys(extendWhere).length > 0){
                    builder.where(extendWhere)
                }
            });
            callback(null, books)
        } catch (e) {
            callback(e, null)
        }
    }
    ,create : async (db, data = {}, callback) => {
        try{
            await db(tableName).insert(data)
            callback(null, [data])
        } catch (e) {
            callback(e, null)
        }
    }
    ,update : async (db, extendWhere = {}, data = {}, callback) => {
        try{
            await db(tableName).where(extendWhere).update(data)
            callback(null, [extendWhere, data])
        } catch (e) {
            callback(e, null)
        }
    }    
    ,delete : async (db, extendWhere = {}, callback) => {
        try{
            await db(tableName).where(extendWhere).del()
            callback(null, [extendWhere])
        } catch (e) {
            callback(e, null)
        }
    }

}