const router = require('express').Router()
const passport = require("passport")
const response = require('../helpers/response_parser')

router.all('/books*', function(req, res, next) {
    passport.authenticate('jwt', function(err, user, info) {
        if (err) { 
            response.error(req, res, err, {status:503})
        }
        if (!user) { 
            response.error(req, res, {message: 'Unauthorized'}, {status:401}) 
        }
        next()
    })(req, res, next);
});

module.exports = router