const db = require('./db')
const passport =  require("passport-jwt")
const JwtStrategy = passport.Strategy
const ExtractJwt = passport.ExtractJwt
const config = require("./config")

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = config.PRIVATE_KEY

module.exports = passport => {
  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      db(config.TABLE_USER_API).where({id: jwt_payload.id}).then(user =>{
        if (user) {
            return done(null, user);
        }
        return done(null, false);
      }).catch(err => console.log(err));      
    })
  );
};