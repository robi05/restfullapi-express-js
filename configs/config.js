module.exports = {
    PRIVATE_KEY: '$2a$10$cT.gIjKBzKw1CdX.Pg2c3.2OEWa7Tzm.Y9CBURI97wKg.U5gb4QHO'
    ,TOKEN_EXPIRED: 3600 //1 hour
    ,TABLE_USER_API: 'apis'
}