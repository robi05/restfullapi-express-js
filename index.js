const express = require("express")
const app = express()
const port = 3000
const db = require('./configs/db')
const bcrypt = require("bcryptjs");
const passport = require("passport");
const oAuthRouter = require('./routers/oauth')
const middleware = require('./configs/middleware')
const booksRouter = require('./routers/books')

app.use(express.json())
app.use(express.urlencoded({extended:true}))

const passingDb = function (req, res, next) {
    req.db = db
    next()
}

app.use(passingDb)
app.use(middleware)

app.get('/',(req, res) =>{
    res.send('Restfull API using Express JS (Node.js) & MySQL (Knex) with JWT')
})

app.use(passport.initialize());
require("./configs/passport")(passport);

app.use(oAuthRouter)
app.use(booksRouter)

app.listen(port, () => {
    console.log(`Server is okay http://localhost:${port}`)
})