-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2021 at 04:23 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `express_restfullapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` varchar(50) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author`, `created_at`, `updated_at`, `deleted_at`) VALUES
('008ef3ae-3c06-41f7-a7a2-613fb3486a6b', 'Book 1', 'Robi', '2021-07-01 09:40:52', NULL, NULL),
('5917895f-2ca0-4aaa-8406-a71d79ed71b3', 'Book Update Ok', 'Robi Saepul', '2021-07-01 09:40:52', '2021-07-01 21:15:27', '2021-07-01 21:15:26'),
('ae0ebe05-a81b-4b7e-abb8-bcddb7a775c5', 'Book 3', 'Saepul Robi', '2021-07-01 13:33:32', NULL, NULL),
('b4208c5b-2968-477e-9b27-b26030764427', 'Book 8', 'Saepul Robi', '2021-07-01 14:01:49', NULL, NULL),
('6fe61fd0-8630-4014-a0b3-3d61a24614a3', 'Book 4', 'Saepul Robi', '2021-07-01 13:47:33', NULL, NULL),
('13750093-ac81-4bc5-87eb-901bb2563723', 'Book 8', 'Saepul Robi', '2021-07-01 14:03:16', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
