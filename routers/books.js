const router = require('express').Router()
const bookController = require('../controllers/books')

router.get('/books', bookController.get)
router.get('/books/:id', bookController.getByID)
router.post('/books', bookController.create)
router.put('/books/:id', bookController.update)
router.delete('/books/:id/soft', bookController.softDelete)
router.delete('/books/:id/hard', bookController.hardDelete)

module.exports = router