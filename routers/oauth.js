const router = require('express').Router()
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const config = require("../configs/config");
const response = require('../helpers/response_parser')

router.post('/oauth/generateToken',(req, res) =>{
    const client_id = req.body.client_id
    const secret_key = req.body.secret_key
    req.db('apis').where({
        client_id: client_id
    }).then((result) => {
        if (result.length>0){
            const api = result[0]
            bcrypt.compare(secret_key, api.secret_key).then(function(isMatch) {
                if (isMatch) {
                    const payload = {id:api.id, client_id:api.client_id};
                    jwt.sign(
                        payload
                        ,config.PRIVATE_KEY
                        ,{ expiresIn: config.TOKEN_EXPIRED }
                        ,(err, token) => {
                            if (err){
                                console.log(err)
                                response.error(req, res, {message: 'Generate token is Failed!'}, {status:401})
                            }else{
                                response.success(req, res, {token:token})
                            }
                        }
                    )
                } else {
                    response.error(req, res, {message: 'Unauthorized'}, {status:401})
                }
            });
        }else{
            response.error(req, res, {message: 'Not found'}, {status:404})
        }
    }).catch((err) => {
        response.error(req, res, err)
    })
})


// for create hash
router.get('/oauth/createHash/:password',(req, res) =>{
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.params.password, salt, (err, hash) => {
          if (err) throw err;
          res.send(hash)
        });
    });
})

module.exports = router