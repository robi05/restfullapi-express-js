const response={
    status: false
    ,data:[]
    ,message:'Error'
    ,method:null
    ,url:null
}

module.exports = {
    success: (req, res, data) => {
        response.status = true
        response.data = data
        response.message = 'Success'
        response.method = req.method
        response.url = req.url
        res.status(200).json(response)
        res.end()
    }
    ,error: (req, res, err, extendVal = {}) => {
        response.status = false
        response.data = err
        response.message = 'Error'
        response.method = req.method
        response.url = req.url
        res.status(extendVal.status!=null?extendVal.status:500).json(response)
        res.end()
    }
}