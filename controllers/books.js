const { v4: uuidv4 } = require('uuid');
const bookModel = require('../models/books')
const response = require('../helpers/response_parser')

const renderResponse = function (req, res, err, results){
    if (err) {
        response.error(req, res, err)
    }else{
        if (results.length>0){
            response.success(req, res, results)
        }else{
            response.error(req, res, {message: 'Failed'}, {status:404})
        }
    }
}

const getData = function (req, res, extendWhere = {}){
    bookModel.get(req.db, extendWhere, (err, results) =>{
        renderResponse(req, res, err, results)
    })        
}

module.exports = {
    get : (req, res) => {
        getData(req, res)       
    }
    ,getByID : (req, res) => {
        const extendWhere = {
            id: req.params.id
        }
        getData(req, res, extendWhere)       
    }
    ,create : (req, res) => {
        const data = {
            id: uuidv4()
            ,title: req.body.title
            ,author: req.body.author
        }
        bookModel.create(req.db, data, (err, results) => {
            renderResponse(req, res, err, results)
        })
    }
    ,update : (req, res) => {
        const data = {
            title: req.body.title
            ,author: req.body.author
        }
        bookModel.update(req.db, {id: req.params.id}, data, (err, results) => {
            renderResponse(req, res, err, results)
        })
    }
    ,softDelete : (req, res) => {
        const data = {
            deleted_at: new Date()
        }
        bookModel.update(req.db, {id: req.params.id}, data, (err, results) => {
            renderResponse(req, res, err, results)
        })
    }
    ,hardDelete : (req, res) => {
        bookModel.delete(req.db, {id: req.params.id}, (err, results) => {
            renderResponse(req, res, err, results)
        })
    }
}